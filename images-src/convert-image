#!/usr/bin/env -S run-cargo-script
//! ```cargo
//! [package]
//! edition = "2018"
//! [dependencies]
//! png = "0.16.7"
//! itertools = "0.9.0"
//! ```

// Note: requires Rust and cargo-script to be installed.
// To install Rust, follow:
// https://www.rust-lang.org/tools/install
// Then run:
// cargo install cargo-script

// Read a png file on stdin and emit JavaScript code on stdout.
// The emitted code should work to include it in a smolpxl game.
// Example:
// ./convert-image < myfile.png > tmp.js

// To format this code:
// rustfmt --config max_width=80 convert-image

use itertools::Itertools;
use std::collections::BTreeMap;
use std::fs::File;
use std::io;
use std::iter::FromIterator;

fn main() -> io::Result<()> {
    for arg in std::env::args().skip(1) {
        process_file(arg)?;
    }
    Ok(())
}

fn process_file(file_name: String) -> io::Result<()> {
    let mut fl = String::from(&file_name);
    fl.truncate(fl.len() - 4);
    let decoder = png::Decoder::new(File::open(file_name)?);
    let (info, mut reader) = decoder.read_info()?;
    let mut buf = vec![0; info.buffer_size()];
    reader.next_frame(&mut buf).unwrap();
    let info = reader.info();

    if info.bit_depth != png::BitDepth::Eight {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            "Currently I can only understand PNGs with a bit depth of 8.",
        ));
    }

    if info.color_type != png::ColorType::RGB
        && info.color_type != png::ColorType::RGBA
    {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            "Currently I can only understand PNGs with a color type \
            of RGB or RGBA.",
        ));
    }

    let pixels = extract_pixels(&info, &buf)?;
    let (letters, colors) = classify_colors(pixels);
    let strings = letters_into_rows(letters, info);

    println!(r#"    "{}": {{"#, fl);
    println!(
        "        \"pixels\": [\n            \"{}\"\n        ],",
        strings.join("\",\n            \"")
    );
    println!(
        "        \"key\": {{\n        {}\n        }}\n    }},",
        colors
            .iter()
            .map(|(le, arr)| format!(r#"    "{}": {:?}"#, le, arr))
            .join(",\n        ")
    );

    Ok(())
}

fn letters_into_rows(
    letters: Vec<Option<String>>,
    info: &png::Info,
) -> Vec<String> {
    let with_dots: Vec<String> = letters
        .into_iter()
        .map(|ch| match ch {
            Some(c) => c,
            None => String::from("."),
        })
        .collect();

    with_dots
        .chunks_exact(info.width as usize)
        .map(|row| row.join(""))
        .collect()
}

struct LetterGenerator {
    i: u8,
}

impl LetterGenerator {
    fn new() -> LetterGenerator {
        LetterGenerator { i: 0 }
    }
    fn next(&mut self) -> String {
        self.i += 1;
        String::from_utf8_lossy(&[97 + ((self.i - 1) % 26)]).to_string()
    }
}

fn classify_colors(
    pixels: Vec<Option<[u8; 3]>>,
) -> (Vec<Option<String>>, BTreeMap<String, [u8; 3]>) {
    let mut letter_gen = LetterGenerator::new();
    let mut letters = vec![];
    let mut color_to_letter = BTreeMap::new();
    for pixel in pixels {
        if let Some(p) = pixel {
            let letter: &str = color_to_letter
                .entry(p)
                .or_insert_with(|| letter_gen.next());
            letters.push(Some(String::from(letter)));
        } else {
            letters.push(None);
        }
    }

    let colors = BTreeMap::from_iter(
        color_to_letter
            .into_iter()
            .map(|(color, letter)| (letter, color)),
    );

    (letters, colors)
}

fn extract_pixels(
    info: &png::Info,
    buf: &Vec<u8>,
) -> io::Result<Vec<Option<[u8; 3]>>> {
    // Obviously, we should not have to built a Vec here - we should
    // be able to return an iterator, but that has proved tricky so
    // here is the best I can do today ;-)
    let bytes_per_pixel = info.bytes_per_pixel();
    let mut ret = vec![];
    let mut err = None;
    let mut pixel_count = 0;
    buf.iter()
        .batching(|it| match it.next() {
            None => None,
            Some(r) => {
                pixel_count += 1;
                let g = it.next().unwrap();
                let b = it.next().unwrap();
                if bytes_per_pixel == 3 {
                    ret.push(Some([*r, *g, *b]));
                    Some(())
                } else if bytes_per_pixel == 4 {
                    let a = it.next().unwrap();
                    if *a == 255 {
                        ret.push(Some([*r, *g, *b]));
                        Some(())
                    } else if *a == 0 {
                        ret.push(None);
                        Some(())
                    } else {
                        err = Some(io::Error::new(
                            io::ErrorKind::Other,
                            format!(
                                "I can only handle fully-transparent or \
                            fully-opaque pixels.  This image has some \
                            semi-transparent pixels, which don't work \
                            in Smolpxl, sorry.  Pixel: {}",
                                pixel_count
                            ),
                        ));
                        None
                    }
                } else {
                    err = Some(io::Error::new(
                        io::ErrorKind::Other,
                        format!(
                            "Unexpected bytes per pixel value: {}",
                            bytes_per_pixel
                        ),
                    ));
                    None
                }
            }
        })
        .for_each(|_| {}); // Consume the iterator but do nothing with it

    if let Some(e) = err {
        Err(e)
    } else {
        Ok(ret)
    }
}
/* vim: set filetype=rust : */
