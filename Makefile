SMOLPXL_JS_VERSION := v0.6.0
DOWNLOAD_URL := https://gitlab.com/smolpxl/smolpxl/-/raw

all:
	@echo Try opening public/index.html in a web browser.

download-smolpxl-js:
	curl --silent --output public/smolpxl.js \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.js"
	curl --silent --output public/smolpxl.css \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.css"
